<?php
include('header.php');
if(isset($_SESSION['alreadyLogged']))
{
    $user = $db->getUserById($_SESSION['email']);
}

if(isset($_POST['makeOrder']))
{
    $order = $db->makeOrder($user->getIdUser(),$_SESSION['totalpriceWithShipping']);

    foreach ($shoppingCart->getAllProductInCart() as $id => $item)
    {
        $db->fillOrder($shoppingCart->getAllProductInCart()[$id]['id'],$order->getIdOrder(),$shoppingCart->getAllProductInCart()[$id]['amount']);
    }

}

?>




<main class="mt-5 pt-4">
    <div class="container wow fadeIn">
        <div class="row justify-content-center">
            <div class="col-md-10">

                <div class="btn-group btn-group-sm ">
                    <a href="ShoppingCart.php" class="btn btn-default">
                        <span class="fa fa-chevron-left">Spät na košík</span>
                    </a>
                </div>
                <div class="row">
                    <div class="col-md-7 mb-4">
                        <div class="card">
                            <h4 class="mt-3 h4 text-center">Vaša fakturačná adresa</h4>
                            <form class="card-body" method="post">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="md-form ">
                                            <?php if(isset($_SESSION['alreadyLogged'])) { ?>
                                            <input type="text" id="firstName" class="form-control" value="<?php echo $user->getName(); ?>" required>
                                            <?php } else {?>
                                                <input type="text" id="firstName" class="form-control" value="" required>
                                            <?php } ?>
                                            <label for="firstName" class="">Meno</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <div class="md-form">
                                            <?php if(isset($_SESSION['alreadyLogged'])) { ?>
                                                <input type="text" id="surname" class="form-control" value="<?php echo $user->getSurname(); ?>" required>
                                            <?php } else {?>
                                                <input type="text" id="surname" class="form-control" value="" required>
                                            <?php } ?>
                                            <label for="surname" class="">Priezvisko</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form mb-3">
                                    <?php if(isset($_SESSION['alreadyLogged'])) { ?>
                                        <input type="text" id="email" class="form-control" value="<?php echo $user->getEmail(); ?>" required>
                                    <?php } else {?>
                                        <input type="text" id="email" class="form-control" value="" required>
                                    <?php } ?>
                                    <label for="email" class="">Email </label>
                                </div>

                                <div class="md-form mb-3">
                                    <?php if(isset($_SESSION['alreadyLogged'])) { ?>
                                        <input type="text" id="street" class="form-control" value="<?php echo $user->getStreet(); ?>" required>
                                    <?php } else {?>
                                        <input type="text" id="street" class="form-control" value="" required>
                                    <?php } ?>
                                    <label for="street" class="">Ulica a číslo</label>
                                </div>


                                <div class="row">
                                    <div class="col-lg-8 col-md-6 mb-3">
                                        <?php if(isset($_SESSION['alreadyLogged'])) { ?>
                                            <input type="text" id="city" class="form-control" value="<?php echo $user->getCity(); ?>" required>
                                        <?php } else {?>
                                            <input type="text" id="city" class="form-control" value="" required>
                                        <?php } ?>
                                        <label for="city" class="">Mesto</label>
                                    </div>

                                    <div class="col-lg-4 col-md-6 mb-3">

                                        <?php if(isset($_SESSION['alreadyLogged'])) { ?>
                                            <input type="text" id="psc" class="form-control" value="<?php echo $user->getPsc(); ?>" required>
                                        <?php } else {?>
                                            <input type="text" id="psc" class="form-control" value="" required>
                                        <?php } ?>
                                        <label for="psc">PSC</label>
                                        <div class="invalid-feedback">
                                            Zip code required.
                                        </div>
                                    </div>
                                </div>


<!--                                <hr>-->

<!--                                <div class="custom-control custom-checkbox">-->
<!--                                    <input type="checkbox" class="custom-control-input" id="save-info">-->
<!--                                    <label class="custom-control-label" for="save-info">Uložiť údaje do profilu</label>-->
<!--                                </div>-->

                                <hr>

                                <div class="d-block my-3">
                                    <div class="custom-control custom-radio">
                                        <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                                        <label class="custom-control-label" for="credit">Karta (0,00 €)</label>
                                    </div>
                                    <div class="custom-control custom-radio">
<!--                                        <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required>-->
                                        <label class="custom-control-label" for="debit"><s>Dobierka (1,99 €)</s>(Vzhľadom na situáciu spojenú s Covid-19, nieje možné objednávať na dobierku)</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="cc-name">Meno držitela karty</label>
                                        <input type="text" class="form-control" id="cc-name" placeholder="" required>
                                        <small class="text-muted">Meno držitela karty</small>
                                        <div class="invalid-feedback">
                                            Name on card is required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="cc-number">Číslo karty</label>
                                        <input type="text" class="form-control" id="cc-number" placeholder="" required>
                                        <div class="invalid-feedback">
                                            Credit card number is required
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <label for="cc-expiration">Dátum</label>
                                        <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
                                        <div class="invalid-feedback">
                                            Expiration date required
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="cc-expiration">CVV</label>
                                        <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                                        <div class="invalid-feedback">
                                            Security code required
                                        </div>
                                    </div>
                                </div>
                                <hr class="mb-4">
                                <input  class="btn cart px-auto" type="submit" name="makeOrder" value="Dokončiť objednávku">
                            </form>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="shoping-cart-overall">
                            <h4>Tvoja objednávka</h4>
                            <hr>
                            <?php
                            if(isset($_SESSION['cart_item'])) {
                            foreach ($shoppingCart->getAllProductInCart() as $id => $item) {
                                $photo = $db->getPhotosByIdProduct($shoppingCart->getAllProductInCart()[$id]['id']);
                                ?>
                            <div class="row cart-detail">
                                <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">

                                    <img src="img/<?php echo $shoppingCart->getAllProductInCart()[$id]['id_category'] ?>/<?php echo $shoppingCart->getAllProductInCart()[$id]['id']?>/<?php echo $photo[0]->getFile() ?>" alt=\"\">
                                </div>
                                <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                    <p><?php echo "" . $shoppingCart->getAllProductInCart()[$id]['name'] . ""; ?></p>
                                    <span
                                            class="price text-info"><?php echo "" . $shoppingCart->getAllProductInCart()[$id]['price'] . ""; ?> €</span>
                                    <span class="count"> 1</span>
                                </div>
                            </div>
                            <?php } }?>
                        </div>

                        <div class="shoping-cart-overall">
                            <table class="tab">
                                <tr class="sumary">
                                    <td class="sumary-a">Suma za produkty</td>
                                    <td class="sumary-b"><?php echo "". $_SESSION['totalprice'] ."" ?> €</td>
                                </tr>
                                <tr class="sumary">
                                    <td>Doprava</td>
                                    <td class="sumary-b"><?php echo "". $_SESSION['shipping'] ."" ?> €</td>
                                </tr>
                                <tr class="sumary bcf">
                                    <td>Poplatok (platba kartou)</td>
                                    <td class="sumary-b">0,00 €</td>
                                </tr>

                                <tr class="gh">
                                    <td class="sumary-a "><strong>Celková suma</strong> </td>
                                    <td class="sumary-b"><strong><?php echo "". $_SESSION['totalpriceWithShipping'] ."" ?> €</strong></td>
                                </tr>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>



