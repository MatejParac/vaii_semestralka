<?php

include ("app/classes/ShoppingCart.php");
$shoppingCart = new ShoppingCart();
$db = new DbStorage();
if(!isset($_SESSION))
{
    session_start();
}
//if(isset($_SESSION['email']))
//{
//    $stmt = $connection->prepare("select * from users where email =  '".$_SESSION['email']."' ");
//    $stmt->execute();
//    $count = $stmt->rowCount();
//    $row   = $stmt->fetch(PDO::FETCH_ASSOC);
//}

?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <title>E-shop</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!--    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"-->
<!--            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>-->
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" type="image/x-icon" href="https://cpwebassets.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico">



</head>
<body>

<script>
    function openLogin() {
        document.getElementById("myLogin").style.display = "block";
    }

    function closeLogin() {
        document.getElementById("myLogin").style.display = "none";
    }

</script>

<header class="sticky-top">
<!--    role="toolbar"-->
    <nav class="navbar fixed-top navbar-dark bg-dark d-block d-lg-none hhh"  id="mobileTopNavbar" style="">
        <ul class="list-inline float-right mb-0 topMobileNav">
            <li class="list-inline-item header-icon">
                <div class="dropdown">
                    <div class="dropdown-cart-a">
                        <button type="button" class="btn " data-toggle="dropdown">
                            <i class="fa fa-user" ></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <?php
                            if(isset($_SESSION['alreadyLogged'])) {
                                echo "
                                                        <div class=\"row justify-content-center log-div\">
                                                            Ste prihlásený.
                                                        </div>
                                                    
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-12 col-sm-12 col-12 text-center checkout\">
                                                                <a href=\"Profil.php\" class=\"btn cart px - auto\" >                                                                   
                                                                    Zobraziť profil
                                                                </a>
                                                            </div>
                                                        </div>                                                
                                                    
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-12 col-sm-12 col-12 text-center checkout\">
                                                                <a href=\"logout.php\" class=\"btn cart px - auto\" >
                                                                    Odhlásiť
                                                                </a>
                                                            </div>
                                                        </div>
                                                    ";
                            }
                            else {
                                echo "
                                                        <div class=\"row justify-content-center log-div\">
                                                            Zatiaľ nieste prihlásený.
                                                        </div>
        
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-12 col-sm-12 col-12 text-center checkout\">
                                                                <a href=\"login.php\" class=\"btn cart px-auto\" >
                                                                    Prihlásiť sa
                                                                </a>
                                                            </div>
                                                        </div>
                                                    
                                                    ";
                            }
                            ?>


                        </div>
                    </div>
                </div>
            </li>
            <li class="list-inline-item header-icon">
                <div class="dropdown">
                    <div class="dropdown-cart-a">
                        <button type="button" class="btn " data-toggle="dropdown">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger"> <?php  echo "". $shoppingCart->getCountOfProduct() .""  ?></span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <?php
                            if(isset($_SESSION['cart_item'])) { ?>
                                <div class="row total-header-section">
                                    <div class="col-lg-6 col-sm-6 col-6">

                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                        <p>Celkovo: <span class="text-info"><?php echo "" .  $_SESSION['totalprice'] . ""; ?> €</span></p>
                                    </div>
                                </div>
                                <?php
                                foreach ($shoppingCart->getAllProductInCart() as $id => $item) {
                                    $photo = $db->getPhotosByIdProduct($shoppingCart->getAllProductInCart()[$id]['id']); ?>
                                    <div class="row cart-detail">
                                        <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                            <img src="img/<?php echo $shoppingCart->getAllProductInCart()[$id]['id_category']."/".$shoppingCart->getAllProductInCart()[$id]['id']."/".$photo[0]->getFile()?>" alt="">
                                        </div>
                                        <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                            <p><?php echo "" . $shoppingCart->getAllProductInCart()[$id]['name'] . ""; ?></p>
                                            <span
                                                    class="price text-info"><?php echo "" . $shoppingCart->getAllProductInCart()[$id]['price']*$shoppingCart->getAllProductInCart()[$id]['amount'] . ""; ?> €</span>
                                            <span class="count"> <?php echo "". $shoppingCart->getAllProductInCart()[$id]['amount'] ."" ?></span>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }else
                            { ?>
                                <div>Tvoj košík je zatiaľ prázdny</div>
                            <?php }?>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                                    <a href="ShoppingCart.php" class="btn cart px-auto" >
                                        Prejsť do košíka
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link hoverable" href="index.php">Domov</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle hoverable" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Výber káv
                    </a>
                    <div class="dropdown-menu drop-menu-fixed" aria-labelledby="navbarDropdownMenuLink">
                        <a href="product.php?category=1 " class="dropdown-item ">Zrnková káva 100% Arabica</a>
                        <a href="product.php?category=2" class="dropdown-item" >Zrnková káva Robusta</a>
                        <a href="product.php?category=3" class="dropdown-item" >Espresso zmesi</a>
                        <a href="product.php?category=4" class="dropdown-item" >Káva rozpustná</a>
                        <a href="product.php?category=5" class="dropdown-item" >Zelená káva</a>
                        <a href="product.php?category=6" class="dropdown-item" >Káva bez kofeinu</a>
                    </div>
                </li>
                <li class="nav-item ">
                    <a class="nav-link hoverable" href="AboutUs.php">O nás</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link hoverable" href="ContactUs.php">Kontakt</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="header-first-div d-none d-lg-block">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-sm-5 ">
                    <div class="row">
                        <div class="col-sm-10">
                            <a href="https://www.facebook.com" class="iconss"><i class="fa fa-facebook site-icon"></i></a>
                            <a href="https://www.instagram.com" class="iconss"><i class="fa fa-instagram site-icon"></i></a>
                            <a href="https://www.twitter.com" class="iconss"><i class="fa fa-twitter site-icon"></i></a>
                        </div>
                    </div>
                </div>


                <div class="col-sm-2 mx-auto text-center">
                    <img src="img/logo2.png" alt="" class="logo-img">
                </div>


                <div class="col-sm-5 ">
                    <div class="row">
                        <div class="col-sm-12 main-section">
                            <ul class="header-icons">
                                <li class="header-icon">
                                    <div class="dropdown">
                                        <div class="dropdown-cart-a">
                                            <button type="button" class="btn " data-toggle="dropdown">
                                                <i class="fa fa-user" ></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <?php
                                                if(isset($_SESSION['alreadyLogged'])) {
                                                    echo "
                                                        <div class=\"row justify-content-center log-div\">
                                                            Ste prihlásený.
                                                        </div>
                                                    
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-12 col-sm-12 col-12 text-center checkout\">
                                                                <a href=\"Profil.php\" class=\"btn cart px - auto\" >
                                                                    Zobraziť profil
                                                                </a>
                                                            </div>
                                                        </div>                                                
                                                    
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-12 col-sm-12 col-12 text-center checkout\">
                                                                <a href=\"logout.php\" class=\"btn cart px - auto\" >
                                                                    Odhlásiť
                                                                </a>
                                                            </div>
                                                        </div>
                                                    ";
                                                }
                                                else {
                                                    echo "
                                                        <div class=\"row justify-content-center log-div\">
                                                            Zatiaľ nieste prihlásený.
                                                        </div>
        
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-12 col-sm-12 col-12 text-center checkout\">
                                                                <a href=\"login.php\" class=\"btn cart px-auto\" >
                                                                    Prihlásiť sa
                                                                </a>
                                                            </div>
                                                        </div>
                                                    
                                                    ";
                                                }
                                                ?>


                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="header-icon">
                                    <div class="dropdown">
                                        <div class="dropdown-cart-a">
                                            <button type="button" class="btn " data-toggle="dropdown">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger"> <?php  echo "". $shoppingCart->getCountOfProduct() .""  ?></span>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right  ">
                                                <?php
                                                if(isset($_SESSION['cart_item'])) { ?>
                                                    <div class="row total-header-section">
                                                        <div class="col-lg-6 col-sm-6 col-6">

                                                        </div>
                                                        <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                                            <p>Celkovo: <span class="text-info"><?php echo "" .  $_SESSION['totalprice'] . ""; ?> €</span></p>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    foreach ($shoppingCart->getAllProductInCart() as $id => $item) {
                                                        $photo = $db->getPhotosByIdProduct($shoppingCart->getAllProductInCart()[$id]['id']); ?>
                                                        <div class="row cart-detail">
                                                            <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                                                <img src="img/<?php echo $shoppingCart->getAllProductInCart()[$id]['id_category']."/".$shoppingCart->getAllProductInCart()[$id]['id']."/".$photo[0]->getFile()?>" alt="">
                                                            </div>
                                                            <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                                                <p><?php echo "" . $shoppingCart->getAllProductInCart()[$id]['name'] . ""; ?></p>
                                                                <span
                                                                        class="price text-info"><?php echo "" . $shoppingCart->getAllProductInCart()[$id]['price']*$shoppingCart->getAllProductInCart()[$id]['amount'] . ""; ?> €</span>
                                                                <span class="count"> <?php echo "". $shoppingCart->getAllProductInCart()[$id]['amount'] ."" ?></span>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }else
                                                { ?>
                                                    <div>Tvoj košík je zatiaľ prázdny</div>
                                                <?php }?>
                                                <div class="row">
                                                    <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                                                        <a href="ShoppingCart.php" class="btn cart px-auto" >
                                                            Prejsť do košíka
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="header-div d-none d-lg-block">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-xs-4 ">
                    <nav class="navbar  navbar-expand-lg navbar-dark ">
                        <div class="collapse navbar-collapse" id="navbarNav1">
                            <ul class="navbar-nav" id="nav-hover">
                                <li class="nav-item">
                                    <a class="nav-link hoverable" href="index.php">Domov</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle hoverable" href="#" id="navbarDropdownMenuLink1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Výber káv
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a href="product.php?category=1 " class="dropdown-item ">Zrnková káva 100% Arabica</a>
                                        <a href="product.php?category=2" class="dropdown-item" >Zrnková káva Robusta</a>
                                        <a href="product.php?category=3" class="dropdown-item" >Espresso zmesi</a>
                                        <a href="product.php?category=4" class="dropdown-item" >Káva rozpustná</a>
                                        <a href="product.php?category=5" class="dropdown-item" >Zelená káva</a>
                                        <a href="product.php?category=6" class="dropdown-item" >Káva bez kofeinu</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>


                <div class="col-xs-4 ">
                    <nav class="navbar navbar-expand-lg navbar-dark  text-center">
                        <div class="collapse navbar-collapse">
                            <ul class="navbar-nav" id="nav-hover1">
                                <li class="nav-item ">
                                    <a class="nav-link hoverable" href="AboutUs.php">O nás</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link hoverable" href="ContactUs.php">Kontakt</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</header>
