<?php
include("header.php");
?>

    <main id="index-main">

        <header class="masthead">
            <div class="container">
                <div class="masthead-subheading">Výberová káva jedine u nás</div>
                <div class="masthead-heading text-uppercase">Coffee shop</div>
                <a class="btn xxa btn-xl text-uppercase js-scroll-trigger" href="#maaain">NÁŠ VÝBER</a>
            </div>
        </header>


        <div style="padding: 30px 0 ; background: black; margin-bottom: 30px">
            <div class="container">
<!--                <div class="row">-->
<!--                    <div class="col-sm-12 ">-->
                        <div class="row justify-content-center ">

                            <div class="col-sm-6 col-lg-3 col-lg-3 col-xxl-5th d-flex justify-content-center  product-card">
                                <div class="xab">
                                    <div class="xa text-center" id="xa">
                                        <div>
                                            <p class="xa-a" style="color:white">Viac o našej káve</p>
                                        </div>
                                        <div class="xg">
                                            <a href="AboutCoffee.php" class="btn cart px-auto">
                                                Zistiť viac
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3 col-lg-3 col-xxl-5th d-flex justify-content-center product-card">
                                <div class="xab text-center">
                                    <div class="xa text-center" id="xb">
                                        <div>
                                            <p class="xa-a">Viac o nás</p>
                                        </div>
                                        <div  class="xg1">
                                            <a href="AboutUs.php" class="btn cart px-auto">
                                                Zistiť viac
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3 col-lg-3 col-xxl-5th d-flex justify-content-center product-card">
                                <div class="xab text-center">
                                    <div class="xa" id="xc">
                                        <div>
                                            <p class="xa-a" style="color: white ">Viac o našej kamennej predajni</p>
                                        </div>
                                        <div  class="xg2">
                                            <a href="storeInfo.php" class="btn cart px-auto" >
                                                Zistiť viac
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3 col-lg-3 col-xxl-5th d-flex justify-content-center product-card">
                                <div class="xab text-center">
                                        <div class="xa" id="xd">
                                            <div>
                                                <p class="xa-a">Kontaktujte nás</p>
                                            </div>
                                            <div class="xg3">
                                                <a href="ContactUs.php" class="btn cart px-auto">
                                                    Zistiť viac
                                                </a>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>

        <div class="container ">
            <div class="row justify-content-center cv">

                <div class="col-sm-12 index-content text-center " id="maaain">
                    <h2 class="h-line"><strong>Naša káva</strong> </h2>
                    <p>
                        Káva je nápoj pripravený z rozomletých pražených semien kávovníka. Ako káva sa označuje aj prášok,
                        ktorý sa na výrobu tohto nápoja používa a získava sa mletím pražených semien kávovníka,
                        označovaných aj ako kávové bôby. Káva je mierne kyslá (5,0 – 5,1 pH) a kofeín, alkaloid, ktorý obsahuje,
                        môže mať stimulačný efekt na ľudský organizmus. Po čaji je káva druhým najkonzumovanejším nápojom na svete.
                    </p>
                    <aside class="pullquote"><q>Aj zlá káva je lepšia ako žiadna káva.</q></aside>
                    <aside class="pullquote" style="float: left"><q>Sila ľudskej mysle je v priamej úmere s množstvom vypitej kávy.</q></aside>
                    <p>
                        Povzbudivé účinky divo rastúcej kávy boli pravdepodobne prvýkrát objavené v severovýchodnych oblastiach Etiópie, odkiaľ pochádza aj jej názov kaffa.
                        Neskôr sa cez Egypt dostala do južnej časti Arabského polostrova, kde sa začalo s jej pestovaním. Prvé vierohodné dôkazy o pití
                        kávy pochádzajú z polovice 15. storočia zo sufijských chrámov v Jemene.[1] Až tu sa káva konzumovala spôsobom, aký poznáme dodnes – pražením
                        semien a ich následným varením. V Európe sa začala stávať populárnou v 17. storočí
                        (prvú európsku kaviareň otvorili v Benátkach v roku 1645), ale pitie kávy sa v Európe stalo masovou záležitosťou až v 19. storočí.
                    </p>
                </div>


                <div class="col-12 index-content text-center" id="Offers">
                    <h2 class="h-line"><strong>NÁŠ VÝBER</strong> </h2>
                </div>

                <div class="col-md-6 col-lg-4 col-lg-4 col-xxl-5th product-card">
                    <div class='text-center'>
                        <a href="product.php?category=1">
                            <img src="img/Kategorie-vyber/arab.jpg" alt="" class="image-coffee">
                        </a>
                        <h3 class="bbv">
                            <a href="product.php?category=6">Arabica</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-lg-4 col-xxl-5th product-card">
                    <div class='text-center'>
                        <a href="product.php?category=2">
                            <img src="img/Kategorie-vyber/robusta.jpg" alt="" class="image-coffee">
                        </a>
                        <h3 class="bbv">
                            <a href="product.php?category=6">Robusta</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-lg-4 col-xxl-5th product-card">
                    <div class='text-center'>
                        <a href="product.php?category=3">
                            <img src="img/Kategorie-vyber/espresso.jpg" alt="" class="image-coffee">
                        </a>
                        <h3 class="bbv">
                            <a href="product.php?category=6">Espresso</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-lg-4 col-xxl-5th product-card">
                    <div class='text-center'>
                        <a href="product.php?category=4">
                            <img src="img/Kategorie-vyber/rozpustna.jfif" alt="" class="image-coffee">
                        </a>
                        <h3 class="bbv">
                            <a href="product.php?category=6">Rozpustná káva</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-lg-4 col-xxl-5th product-card">
                    <div class='text-center'>
                        <a href="product.php?category=5">
                            <img src="img/Kategorie-vyber/green_coffee.jpg" alt="" class="image-coffee">
                        </a>
                        <h3 class="bbv">
                            <a href="product.php?category=6">Zelená káva</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-lg-4 col-xxl-5th product-card">
                    <div class='text-center'>
                        <a href="product.php?category=6">
                            <img src="img/Kategorie-vyber/bezkof.png" alt="" class="image-coffee">
                        </a>
                        <h3 class="bbv">
                            <a href="product.php?category=6">Kava bez kofeinu</a>
                        </h3>
                    </div>
                </div>

            </div>
        </div>

        <div style="margin:20px"> </div>

        <div style="background: black; margin-bottom: 30px">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="find-in-coffee">
                            <div style="margin-bottom:30px ">
                                <h2 class="h-line"><strong>Nájdete v našej kaviarni</strong> </h2>
                            </div>

                            <div class="container-gallery">
                                <div class="popup popup-1">
                                    <img class="img-responsive" alt="Pop Up Gallety" src="https://pacificthaicuisine.com/wp-content/uploads/2019/09/Coffee-Drinks0.jpg" />
                                </div>
                                <div class="popup popup-2">
                                    <img class="img-responsive" alt="Pop Up Gallety" src="https://www.nuggetmarket.com/media/images/07/06/holiday-coffeebar-bakery-pairings-1.jpg" />
                                </div>
                                <div class="popup popup-3">
                                    <img class="img-responsive" alt="Pop Up Gallety" src="https://kcgcorporation.com/wp-content/uploads/2019/01/21.-ChocolateCreamCheeseDrink-740x494.jpg" />
                                </div>
                                <div class="popup popup-4">
                                    <img class="img-responsive" alt="Pop Up Gallety" src="https://images.squarespace-cdn.com/content/v1/56ec8110356fb0c050b7bcca/1533175655225-P7LV3L8MQQ9R2VAUSVU4/ke17ZwdGBToddI8pDm48kKbYNcVTaeH8P2sqBdlIHHt7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0o8OMvY5tuV_wqZQCqqStn7AuPlkxdOrsfwm3VrSLMG3wZJlLN9KeMlrzfnIgCHX4g/KONA+COFFEE+AND+TEA+LATTE" />
                                </div>
                                <div class="popup popup-5">
                                    <img class="img-responsive" alt="Pop Up Gallety" src="https://www.coffeedetective.com/images/coffee-drinks700.png" />
                                </div>
                            </div>
                            <div style="margin-top:30px ">
                                <h2 class="h-line"><strong><img src="img/logo2.png" class="img-find" alt=""></strong> </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php
include("footer.php");
?>