<?php
session_start();
include("app/classes/DbStorage.php");

$msg = "";
$db = new DbStorage();
if(isset($_POST['do_register']))
{
    $email = trim($_POST['email_']);
    $password = trim($_POST['password_']);
    $name = trim($_POST['name_']);
    $surname = trim($_POST['surname_']);

    $msg=$db->registerUser($email,$password,$name,$surname);

    if($msg=="1")
    {
        echo "Ste úspešne zaregistrovaný, prosím prihláste sa";
    }else  {
        echo "Takýto email už je zaregistrovaný";
    }
    exit();
}

?>


<!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
<!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>-->
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords"
          content="unique login form,leamug login form,boostrap login form,responsive login form,free css html login form,download login form">
    <meta name="author" content="leamug">
    <title>E-shop-Registrácia</title>
    <link href="css/style.css" rel="stylesheet" id="style">
    <!-- Bootstrap core Library -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" type="image/x-icon" href="https://cpwebassets.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico">
</head>
<body class="login_body">

<script>
    $(document).ready(function () {
        $("#regButton").on('click',function (e){
            e.preventDefault();
            var email = $("#email").val();
            var password = $("#password").val();
            var name = $("#name").val();;
            var surname = $("#surname").val();;

            if(email=="" || password==""|| name=="" || surname=="")
            {
                $("#response").html("Prosím vyplň všetky polia");
            }else if(!email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/))
            {
                $("#response").html("Zadal si nespravny format emailu");
            }else if (password.length<6)
            {
                $("#response").html("Heslo musí obsahovať aspoň 6 znakov");
            }
            else
            {
                $.ajax(
                    {
                        url: 'register.php',
                        type: 'POST',
                        data: {
                            do_register:1,
                            email_: email,
                            password_: password,
                            name_:name,
                            surname_:surname
                        },
                        success: function (response) {
                            // if(response === "Ste úspešne zaregistrovaný, prosím prihláste sa"){
                            //     // window.location.href = "index.php";
                            //     $("#response").html(response);
                            //    // $('#form-register').trigger("reset");
                            // }else{
                            //     $("#response").html(response);
                            // }
                            $("#response").html(response);

                            if(response === "Ste úspešne zaregistrovaný, prosím prihláste sa") {
                                $('#form-register').trigger("reset");
                            }
                        },
                        dataType:'text'
                    }
                );
            }

        })
    });

</script>


<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-4 text-center rgstr">
            <div class="form-login">
                <div class="lr-list">
                    <a href="login.php" class="btn  btn-lg active" role="button" aria-pressed="true"   id="loginButton">Prihlásenie</a>
                    <a href="#" class="btn  btn-lg active" role="button" aria-pressed="true"  id="registerButton">Registrácia</a>
                </div>
                <form  method="post" id="form-register">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email"  placeholder="*Email"  required>
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="*Heslo (min. 6 znakov)" minlength="6" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Meno" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="surname" id="surname" placeholder="Priezvisko" required>
                    </div>
                    <div class="wrapper">
                            <span class="group-btn">
                                <input  class="btn cart px-auto" type="submit" name="registerButton" id="regButton" value="Registrovat">
                            </span>
                    </div>
                    <p id="response" class="response-logreg"></p>
                </form>

                <a href="index.php" >
                    <span class="fa fa-chevron-left">Pokračovať bez registrácie</span>
                </a>


            </div>
        </div>
    </div>
</div>
</body>
</html>
