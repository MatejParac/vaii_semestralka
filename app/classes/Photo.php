<?php


class Photo
{
    private $id_photo;
    private $file;
    private $id_product;

    public function __construct($id,$file,$id_product)
    {
        $this->id_photo = $id;
        $this->file = $file;
        $this->id_product = $id_product;
    }

    /**
     * @return mixed
     */
    public function getIdPhoto()
    {
        return $this->id_photo;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->id_product;
    }

}