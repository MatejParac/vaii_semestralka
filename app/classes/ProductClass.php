<?php


class ProductClass
{
    private $id;
    private $name;
    private $price;
    private $description;
    private $id_category;

    private $composition;
    private $taste;
    private $origin;
    private $instruction;

    public function __construct($id,$name,$price,$description,$idCategory,$c,$t,$o,$i)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->id_category = $idCategory;
        $this->composition = $c;
        $this->taste = $t;
        $this->origin = $o;
        $this->instruction = $i;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function getPrice()
    {
        return $this->price;
    }


    public function getDescription()
    {
        return $this->description;
    }


    public function getIdCategory()
    {
        return $this->id_category;
    }

    public function getComposition()
    {
        return $this->composition;
    }

    public function getTaste()
    {
        return $this->taste;
    }

    public function getOrigin()
    {
        return $this->origin;
    }

    public function getInstruction()
    {
        return $this->instruction;
    }

}