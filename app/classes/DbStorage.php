<?php
include('ProductClass.php');
include('Category.php');
include ('User.php');
include ('Recension.php');
include ('Photo.php');
include "Order.php";
include "OrderItem.php";
class DbStorage
{
    private $connection;

    public function __construct()
    {
        try {
            $this->connection = new PDO('mysql:dbname=coffeeshop;host=localhost','root','dtb456');
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    public function loginUser($email, $password)
    {
        try {
            $stmt= $this->connection->prepare("select * from users where email = '$email'");
            $stmt->bindValue('email', $email, PDO::PARAM_STR);
            $stmt->execute();
            $count = $stmt->rowCount();
            $row   = $stmt->fetch(PDO::FETCH_ASSOC);
            $msg="";
            if($count == 1 && !empty($row))
            {
                if(password_verify($password,$row['password']))
                {
                    $_SESSION['alreadyLogged'] = TRUE;
                    $_SESSION['email'] = $email;
                    $_SESSION['id_logged_user'] = $row['id_user'];
                    $msg="Uspesne prihlaseny";
                    return $msg;
                }else {
                    $msg = "Zadali ste zle heslo";
                    return $msg;
                }
            }else {
                $msg = "Taky uzivatel neexistuje";
                return $msg;
            }
        } catch (PDOException $e) {
            $msg = "catch";
            return $msg;
        }
    }

    public function registerUser($email,$password,$name,$surname)
    {
        try
        {
            $stmt = $this->connection->prepare("select * from users where email = '$email' ");
            $stmt->bindValue('email', $email, PDO::PARAM_STR);
            $stmt->execute();
            $count = $stmt->rowCount();
            $row   = $stmt->fetch(PDO::FETCH_ASSOC);

            if($count == 1 && !empty($row))
            {
                //Takyto email je uz zaregistrovany
                return $msg="0";
            }else
            {
                $password_crypted = password_hash($password,PASSWORD_DEFAULT);
                $inserted_stmt = $this->connection->prepare("insert into users (password, email, name,surname) values (:upassword, :umail, :uname,:usurname)");

                if($inserted_stmt->execute(array(':upassword' =>$password_crypted , ':umail' => $email , ':uname' => $name,':usurname'=>$surname)))
                {
                    //uspesne zaregistrovany
                    return $msg = "1";
                }else{
                    return $msg= "Nieco zle";
                }
            }
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getUserById($email)
    {
        $stmt = $this->connection->prepare("select * from users where email =  '$email' ");
        $stmt->execute();
        $row   = $stmt->fetch(PDO::FETCH_ASSOC);

        return $user = new User($row['id_user'],$row['email'],$row['password'],$row['name'],$row['surname'],$row['street'],$row['city'],$row['psc']);
    }

    public function getUserById2($id)
    {
        $stmt = $this->connection->prepare("select * from users where id_user =  '$id' ");
        $stmt->execute();
        $row   = $stmt->fetch(PDO::FETCH_ASSOC);

        return $user = new User($row['id_user'],$row['email'],$row['password'],$row['name'],$row['surname'],$row['street'],$row['city'],$row['psc']);
    }

    public function getIdUser($email)
    {
        $stmt = $this->connection->prepare("select * from users where email =  '$email' ");
        $stmt->execute();
        $row   = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['id_user'];
    }


    public function setPersonalData($mail,$name,$surname){
        $data = [
            'mail' => $mail,
            'name' => $name,
            'surname' => $surname,
        ];

        $inserted_stmt = $this->connection->prepare("update users set email=:mail,name=:name, surname=:surname where email= '".$_SESSION['email']."' ");

        if($inserted_stmt->execute($data))
        {
            echo '<script type="text/javascript"> window.location = "Profil.php"    </script>';
            $_SESSION['email'] = $mail;
            return $msg = "Data pridane";
        }else{
            return $msg= "Nieco zle";
        }
    }

    public function setFacturAdress($street,$city,$psc) {
        $data = [
            'street' => $street,
            'city' => $city,
            'psc' => $psc
        ];
        $inserted_stmt = $this->connection->prepare("update users set street=:street,city=:city, psc=:psc where email= '".$_SESSION['email']."' ");
        if($inserted_stmt->execute($data))
        {
            echo '<script type="text/javascript"> window.location = "Profil.php"    </script>';
            return $msg = "Data pridane";

        }else{
            return $msg= "Nieco zle";
        }
    }

    public function changePassword($user,$oldPassword,$newPassword,$newPasswordAgain)
    {
        if($newPassword != $newPasswordAgain)
        {
            return $msg="Hesla sa nezhoduju";
        }elseif (!password_verify($oldPassword,$user->getPassword()))
        {
            return $msg= "Vase stare heslo je nespravne.";
        }
        else{
            $password_crypted = password_hash($newPassword,PASSWORD_DEFAULT);
            $data = [
                'password' => $password_crypted
            ];
            $inserted_stmt = $this->connection->prepare("update users set password=:password where email= '".$_SESSION['email']."' ");
            if($inserted_stmt->execute($data))
            {
                $msg = "Heslo uspesne zmenene";
                echo '<script type="text/javascript"> window.location = "Profil.php"</script>';
            }else{
                return $msg= "Nieco zle";
            }
        }
    }

    public function getProducts($idCategory)
    {
        $products = $this->connection->query("select * from products where id_category =' $idCategory '");
        $productArray=[];

        foreach ($products as $product)
        {
            $productArray[] = new ProductClass($product['id_product'], $product['name'],$product['price'],
                $product['description'],$product['id_category'],$product['composition'],$product['taste'],$product['origin'],$product['instruction']);
        }

        return $productArray;
    }

    public function getProductById($idProduct)
    {
        $stmt = $this->connection->query("select * from products where id_product =' $idProduct '");
        $stmt->execute();
        $row   = $stmt->fetch(PDO::FETCH_ASSOC);

        return $product = new ProductClass($row['id_product'], $row['name'],$row['price'],$row['description'],$row['id_category']
            ,$row['composition'],$row['taste'],$row['origin'],$row['instruction']);
    }

    public function getCategories()
    {

    }

    public function getCategoryById($idCategory)
    {
        $stmt = $this->connection->prepare("select * from categories where id_category = '$idCategory' ;");
        $stmt->execute();
        $count = $stmt->rowCount();
        $row   = $stmt->fetch(PDO::FETCH_ASSOC);
        return $category = new Category($row['id_category'],$row['name'],$row['category_desc'],$row['desc1'],$row['desc2'],$row['desc3']);
    }

    public function getNameOfCategory($id)
    {
        $stmt = $this->connection->prepare("select name from categories where id_category = '$id' ;");
        $stmt->execute();
        $row   = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['name'];
    }

    public function getRecensionsById($id)
    {
        $recensions = $this->connection->query("select * from recensions where id_product =' $id '");
        $recensionstArray=[];

        foreach ($recensions as $recension)
        {
            $recensionstArray[] = new Recension($recension['id_recenzie'], $recension['text'],$recension['id_user'],$recension['id_product'],$recension['date']);
        }

        return $recensionstArray;
    }

    public function getPhotosByIdProduct($idProduct)
    {
        $photos = $this->connection->query("select * from photos where id_product =' $idProduct '");
        $photosArray=[];

        foreach ($photos as $photo)
        {
            $photosArray[] = new Photo($photo['id'], $photo['file'],$photo['id_product']);
        }

        return $photosArray;
    }

    public function makeOrder($id_user,$total_price)
    {
        $inserted_stmt = $this->connection->prepare("insert into orders (id_user, total_price) values (:uiduser, :utotal_price)");

        if($inserted_stmt->execute(array(':uiduser' => $id_user ,':utotal_price' => $total_price)))
        {
            $stmt = $this->connection->prepare("select * from orders ORDER BY id_order DESC LIMIT 1;");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return new Order($row['id_order'],$id_user,$total_price);
        }else{
            return $msg= "Nieco zle";
        }
    }

    public function fillOrder($id_product,$id_order,$amount)
    {
        $inserted_stmt = $this->connection->prepare("insert into order_items (id_product, id_order,amount) values (:uidproduct, :uid_product,:uamount)");

        if($inserted_stmt->execute(array(':uidproduct' => $id_product ,':uid_product' => $id_order, ':uamount' => $amount)))
        {
            return $msg = "Item pridany";
        }else{
            return $msg= "Nieco zle";
        }
    }

    public function getOrders($id_user)
    {
        $orders = $this->connection->query("select * from orders where id_user ='$id_user'");
        $ordersArray=[];

        foreach ($orders as $order)
        {
            $ordersArray[] = new Order($order['id_order'], $order['id_user'],$order['total_price']);
        }

        return $ordersArray;
    }

    public function getOrderItems($id_order)
    {
        $orderItems = $this->connection->query("select * from order_items where id_order ='$id_order'");
        $orderItemsArray=[];
        foreach ($orderItems as $item)
        {
            $orderItemsArray[] = new OrderItem($item['id_order_item'],$item['id_product'],$item['id_order'],$item['amount']);
        }

        return $orderItemsArray;
    }

    public function setRecension($id_product,$id_user,$text,$date)
    {
        $inserted_stmt = $this->connection->prepare("insert into recensions (id_product,id_user,text,date) values (:uid_product,:uid_user,:utext,:udate)");

        if ($inserted_stmt->execute(array(':uid_product' => $id_product, ':uid_user' => $id_user, ':utext' => $text,':udate' => $date))) {
            echo '<script type="text/javascript"> window.location = "detail.php"</script>';
        } else {
            $msg = "Nieco zle";
        }
    }

    public function deleteRecension($id)
    {
//        $deleted_stmt = $this->connection->query("delete from recensions where id_recenzie=$id");
        $deleted_stmt = "delete from recensions where id_recenzie=$id";
        if($this->connection->exec($deleted_stmt))
        {
            //echo "Vymazane";
            return true;
        }else return false;
    }
}