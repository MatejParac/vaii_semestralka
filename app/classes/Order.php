<?php


class Order
{
    private $id_order;
    private $id_user;
    private $total_price;

    public function __construct($id_order,$id_user,$total_price)
    {
        $this->id_order = $id_order;
        $this->id_user = $id_user;
        $this->total_price = $total_price;
    }

    public function getIdOrder()
    {
        return $this->id_order;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }


}