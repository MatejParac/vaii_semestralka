<?php
include("app/classes/DbStorage.php");
$db = new DbStorage();
class ShoppingCart
{

    public function addToShoppingCart($product,$amount)
    {
        $data = array($product->getId() =>
            array('id' =>$product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'id_category' => $product->getIdCategory(),
                'amount' => $amount)
        );
        if(!empty($_SESSION["cart_item"]))
        {
            $_SESSION["cart_item"] += $data;
        }else{
            //$_SESSION["cart_item"] = array();
            $_SESSION["cart_item"] = $data;
        }
        $_SESSION['totalprice']+= $product->getPrice()*$amount;
    }

    public function getCountOfProduct()
    {
        if(isset($_SESSION["cart_item"]))
        {
            return count($_SESSION['cart_item']);
        }else
            return 0;

    }

    public function getAllProductInCart()
    {
        if(isset($_SESSION["cart_item"]))
        {
            return $_SESSION["cart_item"];
        }else
            return 0;
    }

    public function removeProductFromCart($id)
    {
        if(isset($_SESSION['cart_item']))
        {
            if(count($_SESSION['cart_item'])==1)
            {
                unset($_SESSION['cart_item']);
            }
            else{
                $_SESSION['totalprice']-= $_SESSION['cart_item'][$id]['price']*$_SESSION['cart_item'][$id]['amount'];
                unset($_SESSION['cart_item'][$id]);
            }

        }
    }

    public function removeCart(){
        if(isset($_SESSION['cart_item']))
        {
            unset($_SESSION['cart_item']);
            $_SESSION['totalprice']=0;
        }
    }
}