<?php


class Recension
{
    private $id_recenzie;
    private $text;
    private $id_user;
    private  $id_product;
    private $date;

    public function __construct($id_recenzie,$text,$id_user,$id_product,$date)
    {
        $this->id_recenzie = $id_recenzie;
        $this->text = $text;
        $this->id_user = $id_user;
        $this->id_product = $id_product;
        $this->date = $date;
    }

    public function getIdRecenzie()
    {
        return $this->id_recenzie;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getIdUser()
    {
        return $this->id_user;
    }

    public function getDate()
    {
        return $this->date;
    }

}