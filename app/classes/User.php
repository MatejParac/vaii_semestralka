<?php


class User
{
    private $id_user;
    private $email;
    private  $password;
    private $name;
    private $surname;
    private $street;
    private $city;
    private $psc;

    public function __construct($id_user,$email,$password,$name,$surname,$street,$city,$psc)
    {
        $this->id_user = $id_user;
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->street = $street;
        $this->city = $city;
        $this->psc = $psc;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getPsc()
    {
        return $this->psc;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    public function setStreet($street)
    {
        $this->street = $street;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function setPsc($psc)
    {
        $this->psc = $psc;
    }

    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }


}