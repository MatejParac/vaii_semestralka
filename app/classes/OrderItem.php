<?php


class OrderItem
{
    private $id;
    private $id_product;
    private $id_order;
    private $amount;

    public function __construct($id,$id_product,$id_order,$amount)
    {
        $this->id = $id;
        $this->id_product = $id_product;
        $this->id_order = $id_order;
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->id_product;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }


}