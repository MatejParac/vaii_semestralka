<?php


class Category
{
    private $id_category;
    private $name;
    private $description;
    private $info1;
    private $info2;
    private $info3;

    public function __construct($id,$name,$description,$info1,$info2,$info3)
    {
        $this->id_category = $id;
        $this->name = $name;
        $this->description = $description;
        $this->info1 = $info1;
        $this->info2 = $info2;
        $this->info3 = $info3;
    }

    public function getIdCategory()
    {
        return $this->id_category;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getInfo1()
    {
        return $this->info1;
    }

    public function getInfo2()
    {
        return $this->info2;
    }

    public function getInfo3()
    {
        return $this->info3;
    }
}