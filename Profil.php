<?php
include('header.php');

$user = $db->getUserById($_SESSION['email']);

if(isset($_POST['submitChangePD']))
{
    $email = trim($_POST['email']);
    $name = trim($_POST['name']);
    $surname = trim($_POST['surname']);

    $msg= $db->setPersonalData($email,$name,$surname);
}

if(isset($_POST['submitChangeFD']))
{
    $street = trim($_POST['street']);
    $city = trim($_POST['city']);
    $psc = trim($_POST['psc']);

    $msg= $db->setFacturAdress($street,$city,$psc);
}

if(isset($_POST['submitChangePassword'])) {

    $oldPassword = trim($_POST['oldPassword']);
    $newPassword = trim($_POST['newPassword']);
    $newPasswordAgain = trim($_POST['newPasswordAgain']);
   // $old_Password_crypted = password_hash($oldPassword,PASSWORD_DEFAULT);

    $msg=$db->changePassword($user,$oldPassword,$newPassword,$newPasswordAgain);

}

//if(isset($_POST['submitRemoveAccount'])) {
//    $stmt = $connection->prepare("DELETE FROM users WHERE id_user = '".$_SESSION['id_logged_user']."' ");
//    $stmt->bindParam(':id', $id);
//    if($stmt->execute()){
//        echo '<script type="text/javascript"> window.location = "logout.php"</script>';
//    }else echo '<script type="text/javascript"> window.location = "index.php"</script>';
//}
?>

<script>

    function openPersonalData() {
        document.getElementById("personalData").style.display = "block";
        document.getElementById("profil-main").style.opacity="0.3"
    }

    function openFacturData() {
        document.getElementById("facturData").style.display = "block";
        document.getElementById("profil-main").style.opacity="0.3"
    }

    function openChangePassword() {
        document.getElementById("changePassword").style.display = "block";
        document.getElementById("profil-main").style.opacity="0.3"
    }

    function closeWindow() {
        document.getElementById("facturData").style.display = "none";
        document.getElementById("personalData").style.display = "none";
        document.getElementById("changePassword").style.display ="none";
        document.getElementById("profil-main").style.opacity="1"
    }

</script>

<main id="profil-main">
    <div class="container">
        <div class="row" >
            <div class="col-sm-12 text-center">
                <div style="margin-top:30px ">
                    <h2 class="h-line"><strong><img src="img/logo2.png" class="img-find" alt=""></strong> </h2>
                </div>
            </div>
            <div class="profil-main-panel col-12">
                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="card mb-4">
                            <h3 class="card-header">Ucet</h3>
                            <div class="card-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item" onclick="openPersonalData()"><i class="fa fa-user"></i> OSOBNE UDAJE</a>
                                    <a href="#" class="list-group-item" onclick="openFacturData()"><i class="fa fa-truck"></i> FAKTURACNA ADRESA</a>
                                    <a href="#" class="list-group-item" onclick="openChangePassword()"><i class="fa fa-key"></i> ZMENA HESLA</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card mb-4">
                            <h3 class="card-header">Objednavky</h3>
                            <div class="card-body">
                                <div class="list-group">
                                    <a href="orders.php" class="list-group-item"><i class="fa fa-shopping-cart"></i> MOJE OBJEDNAVKY</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card mb-4">
                            <h3 class="card-header">Odhlasenie</h3>
                            <div class="card-body">
                                <div class="list-group">
                                    <a href="logout.php" class="list-group-item"><i class="fa fa-sign-out"></i> ODHLASIT SA</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="profil-data"  id="personalData">
    <div class="container">
        <div class="row ">
            <div class="col-sm-12">
                <form  class="settings-container" method="post" >

                    <div class="row">
                        <div class="col-sm-11 my-auto">
                            <h4 style="text-align: center">Zmena osobnych udajov</h4>
                        </div>
                        <div class="col-sm-1 my-auto">
                            <button type="button" class="close" aria-label="Close" onclick="closeWindow()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <ul class="settings-list">
                        <li class=" settings-anchor">
                            <span class="data-span">Meno:</span>
                            <input type="text" class="form-control" name="name" id="name"  value="<?php echo $user->getName(); ?>">
                        </li>
                        <li class="settings-anchor">
                            <span class="data-span">Priezvisko:</span>
                            <input type="text" class="form-control" name="surname" id="surname"  value="<?php echo $user->getSurname(); ?>">
                        </li>
                        <li class="settings-anchor">
                            <span class="data-span">Mail:</span>
                            <input type="email" class="form-control" name="email" id="email"  value="<?php echo $user->getEmail(); ?>" required>
                        </li>
                    </ul>
                    <input  class="btn cart px-auto" type="submit" name="submitChangePD" value="Zmenit">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="profil-data" id="facturData">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <form  class="settings-container" method="post" >
                    <div class="row">
                        <div class="col-sm-11 my-auto">
                            <h4 style="text-align: center">Zmena fakturacnych udajov</h4>
                        </div>
                        <div class="col-sm-1 my-auto">
                            <button type="button" class="close" aria-label="Close" onclick="closeWindow()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <ul class="settings-list">
                        <li class=" settings-anchor">
                            <span class="data-span">Ulica:</span>
                            <input type="text" class="form-control" name="street" id="street"  value="<?php echo $user->getStreet(); ?>">
                        </li>
                        <li class="settings-anchor">
                            <span class="data-span">Mesto:</span>
                            <input type="text" class="form-control" name="city" id="city" value="<?php echo $user->getCity(); ?>">
                        </li>
                        <li class="settings-anchor">
                            <span class="data-span">PSC:</span>
                            <input type="text" class="form-control" name="psc" id="psc"  value="<?php echo $user->getPsc(); ?>">
                        </li>
                    </ul>
                    <input  class="btn cart px-auto" type="submit" name="submitChangeFD" value="Zmenit">
                    <p><?php $msg ?></p>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="profil-data" id="changePassword">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form  class="settings-container" method="post">
                    <div class="row">
                        <div class="col-sm-11 my-auto">
                            <h4 style="text-align: center">Zmena hesla</h4>
                        </div>
                        <div class="col-sm-1 my-auto">
                            <button type="button" class="close" aria-label="Close" onclick="closeWindow()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <ul class="settings-list">
                        <li class=" settings-anchor">
                            <span class="data-span">Zadaj svoje terajsie heslo</span>
                            <input type="password" class="form-control" name="oldPassword" id="oldPassword" placeholder="Heslo" required>
                        </li>
                        <li class="settings-anchor">
                            <span class="data-span">Zadaj nove heslo</span>
                            <input type="password" class="form-control" name="newPassword" id="newPassword" placeholder="Heslo" required>
                        </li>
                        <li class="settings-anchor">
                            <span class="data-span">Zadaj znova heslo</span>
                            <input type="password" class="form-control" name="newPasswordAgain" id="newPasswordAgain" placeholder="Heslo" required>
                        </li>
                    </ul>
                    <input  class="btn cart px-auto" type="submit" name="submitChangePassword" value="Zmenit">
                </form>

            </div>
        </div>
    </div>
</div>

