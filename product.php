<?php
include("header.php");

if(isset($_GET['category']))
{
    $_SESSION['idCategory'] = $_GET['category'];
}
$category = $db->getCategoryById($_SESSION['idCategory']);

if(isset($_GET['code']))
{
    $product = $db->getProductById($_GET['code']);
    $shoppingCart->addToShoppingCart($product,1);
    echo '<script type="text/javascript"> window.location = "Product.php#b"    </script>';
}

?>

<div class="product-intro">
    <header class="masthead product-<?php echo $category->getIdCategory()?>">
        <div class="container d-flex h-100 align-items-center">
            <div class="mx-auto text-center">
                <h1 class="mx-auto my-0 text-uppercase"><?php echo $category->getName() ?></h1>
                <h2 class="text-white-50 mx-auto mt-2 mb-5"><?php echo $category->getDescription() ?></h2>
                <a class="btn xxa js-scroll-trigger" href="#b">Naša ponuka</a>
            </div>
        </div>
    </header>

    <section class="about-section text-center" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    <h2 class="text-white mb-4">Krátky popis</h2>

                    <p class="text-white-50">
                        <?php echo $category->getInfo1() ?>
                    </p>
                    <p class="text-white-50">
                        <?php echo $category->getInfo2() ?>
                    </p>
                    <p class="text-white-50">
                        <?php echo $category->getInfo3() ?>
                    </p>
                </div>
            </div>
            <img class="img-fluid" src="img/123.png" alt="" id="b"/>
        </div>
    </section>

</div>


<div class="container" >
    <div class="col-12 index-content text-center">
        <h2 class="h-line"><strong>NAŠA PONUKA</strong> </h2>
    </div>

    <div class="row coffee-main-panel justify-content-center" >
        <?php
        $products =[];
        $products = $db->getProducts( $_SESSION['idCategory']);

        if(count($products) >0){
            foreach ($products as $product) {
                $photo = $db->getPhotosByIdProduct($product->getId());
                echo
                    "                                      
                        <div class=\"col-sm-6 col-md-4 col-lg-3 col-xxl-5th product-card\">  
                            <div class='text-center'>
                                
                                <a class='bbh' href=\"detail.php?product=" . $product->getId() . "\" >
                                    <img src=\"img/".$product->getIdCategory()."/".$product->getId()."/".$photo[0]->getFile()."\" alt=\"\" class=\"image-product\">
                                </a>
                                <h6>
                                    <a href=\"detail.php?product=" . $product->getId() . "\">" . $product->getName() . "</a>
                                </h6>
                                <a href=\"product.php?code=" . $product->getId() ."\" >
                                     <input  class=\"btn cart px-auto\" type=\"submit\" name=\"addToCart\" value=\"Pridať do košíka\">
                                </a>
                                 <h4 class=\"font-weight-bold my-2\">" . $product->GetPrice() . " €</h4>
                            </div>                                              
                                
                        </div>
                        ";
            }
        }else
        {
            echo "<p>Momentalne niesu dostupne ziadne produkty</p>";
        }?>

    </div>
</div>

<?php
include("footer.php");
?>
