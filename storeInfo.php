<?php
include "header.php";
?>

<section class="cta">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <div class="cta-inner text-center rounded">
                    <h2 class="section-heading mb-5">
                        <span class="section-heading-upper">Srdečne Vás uvítame</span>
                        <span class="section-heading-lower">Sme otvorení</span>
                    </h2>
                    <ul class="list-unstyled list-hours mb-5 text-left mx-auto">
                        <li class="list-unstyled-item list-hours-item d-flex">
                            Pondelok
                            <span class="ml-auto">9:00 - 22:00</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                            Utorok
                            <span class="ml-auto">9:00 - 22:00</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                            Streda
                            <span class="ml-auto">9:00 - 22:00</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                            Štvrtok
                            <span class="ml-auto">9:00 - 22:00</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                            Piatok
                            <span class="ml-auto">9:00 - 00:00</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                            Sobota
                            <span class="ml-auto">9:00 - 00:00</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                            Nedela
                            <span class="ml-auto">9:00 - 20:00</span>
                        </li>
                    </ul>
                    <p class="address mb-2">
                        <em>
                            <strong>Limbová 9</strong>
                            <br>
                            Žilina
                        </em>
                    </p>
                    <p class="mb-0">
                        <small>
                            <em>Rezervácie môžte uskutočniť na čísle</em>
                        </small>
                        <br>
                        0912 345 678
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <div style="margin-top:30px">
                <h2 class="h-line"><strong><img src="img/logo2.png" class="img-find" alt=""></strong> </h2>
            </div>
        </div>
    </div>
</div>

<div class="container storeInfoImages">
    <div class="row">
        <div class="col-sm-12">
            <ul class="gallery_box">
                <li>
                    <a href="#0"><img src="https://media-cdn.tripadvisor.com/media/photo-s/1a/05/9f/f7/fruit-tree-coffee-shop.jpg" alt="">
                        <div class="box_data">

                        </div></a>
                </li>
                <li>
                    <a href="#0"><img src="https://media-cdn.tripadvisor.com/media/photo-s/10/60/60/48/a-shot-from-our-soft.jpg" alt="">
                        <div class="box_data">

                        </div></a>
                </li>
                <li>
                    <a href="#0"><img src="https://media-cdn.tripadvisor.com/media/photo-s/10/60/60/d2/we-offer-counter-service.jpg" alt="">
                        <div class="box_data">

                        </div></a>
                </li>
                <li>
                    <a href="#0"><img src="https://media-cdn.tripadvisor.com/media/photo-s/1a/05/9f/f7/fruit-tree-coffee-shop.jpg" alt="">
                        <div class="box_data">

                        </div></a>
                </li>
                <li style="    position: relative;
top: -134px;">
                    <a href="#0"><img src="https://www.smithandvallee.com/wp-content/uploads/2017/09/frontcounter.jpg" alt="">
                        <div class="box_data">

                        </div></a>
                </li>
                <li>
                    <a href="#0"><img src="https://331mrnu3ylm2k3db3s1xd1hg-wpengine.netdna-ssl.com/wp-content/uploads/2017/07/Camber-Coffee-Soft-Open-16-1200x800.jpg" alt="">
                        <div class="box_data">

                        </div></a>
                </li>

            </ul>
        </div>
    </div>
</div>



