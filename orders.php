<?php
include "header.php";

if(isset($_SESSION['alreadyLogged']))
{
    $user = $db->getUserById($_SESSION['email']);
}



?>


<div class="container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <div style="margin-top:30px ">
                <h2 class="h-line"><strong>Historia objednavok</strong> </h2>
            </div>
        </div>

        <?php
        $orders = $db->getOrders($user->getIdUser());
        if(count($orders) > 0)
        {
        ?>
        <div class="col-sm-12">
            <div id="accordion">
                <?php

                $i=0;
                foreach ($orders as $order)
                { ?>
                    <div class="card">
<!--                        id="headingOnee"-->
                        <div class="card-header" >

                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link mmm" data-toggle="collapse" data-target="#collapse<?php echo $i?>" aria-expanded="true" aria-controls="collapse<?php echo $i?>">
                                                Objednavka ( číslo objednávky: <?php echo $order->getIdOrder() ?> )
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="col-sm-2 my-auto">
                                        <span class="float-right mmm">
                                            <?php echo $order->getTotalPrice() ?> €
                                        </span>
                                    </div>
                                </div>
                            </div>



                        </div>
<!--                        aria-labelledby="headingOne"-->
                        <div id="collapse<?php echo $i?>" class="collapse "  data-parent="#accordion">
                            <div class="card-body">
                                <?php
                                $orderItems = $db->getOrderItems($order->getIdOrder());
                                foreach ($orderItems as $order_item)
                                {
                                    $product = $db->getProductById($order_item->getIdProduct());
                                    $photo = $db->getPhotosByIdProduct($order_item->getIdProduct());
                                    ?>
                                    <ul class="shoping-cart-ul">
                                        <li class="shoping-cart-item">
                                            <div class="shoping-cart-order">
                                                <div class="shoping-cart-order-details">
                                                    <div class="shoping-cart-image">
                                                        <img src="img/<?php echo $product->getIdCategory() ?>/<?php echo $product->getId() ?>/<?php echo $photo[0]->getFile()?>" alt="">
                                                    </div>

                                                    <div class="shopping-cart-o-d-n">
                                                        <div>
                                                            <div class='shoping-cart-product-name'> <strong><?php echo  $product->getName(); ?> (<?php echo $order_item->getAmount()?>)</strong> </div>
                                                            <div class='shoping-cart-product-name'><?php echo  $db->getProductById($order_item->getIdProduct())->getPrice() ?> € </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-price">
                                                    <div class="s-c-price">
                                                        <?php echo  $db->getProductById($order_item->getIdProduct())->getPrice()*$order_item->getAmount(); ?> €
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
                ?>


            </div>
        </div>
        <?php
        } else { ?>
            <div class="col-sm-12">
                Zatial ste si nic nekupili
                <i class="fa fa-history"></i>

            </div>
        <?php } ?>

    </div>
</div>
