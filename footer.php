<?php
?>

<footer>
    <section class="signup-section" id="signup">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto text-center">
                    <i class="fa fa-paper-plane fa-2x mb-2 text-white"></i>
                    <h2 class="text-white mb-5">Ak sa vyskytol nejaký problém, prosím kontaktujte nás!</h2>
                    <div class="col-sm-12 text-center">
                        <a href="ContactUs.php" class="btn btn-success mx-auto">Kontaktujte nás</a>
                    </div>
                    <form class="form-inline d-flex">

                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact-->
    <section class="contact-section bg-black">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <i class="fa fa-book  text-success mb-2"></i>
                            <h4 class="text-uppercase m-0">Adresa</h4>
                            <hr class="my-4" />
                            <div class="small text-black-50">Limbová 9, Žilina</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <i class="fa fa-envelope text-success mb-2"></i>
                            <h4 class="text-uppercase m-0">Email</h4>
                            <hr class="my-4" />
                            <div class="small text-black-50"><a href="#!">Email@gmail.com</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <i class="fa fa-phone text-success mb-2"></i>
                            <h4 class="text-uppercase m-0">Telefónne číslo</h4>
                            <hr class="my-4" />
                            <div class="small text-black-50">(+421)902 123 456</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>

</body>
</html>