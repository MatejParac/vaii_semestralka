<?php
include "header.php";

if(isset($_GET['product'])) {
    $_SESSION['idProduct'] = $_GET['product'];
}

$product = $db->getProductById($_SESSION['idProduct']);
$recensions = [];
$recensions = $db->getRecensionsById($_SESSION['idProduct']);

$photos = [];
$photos = $db->getPhotosByIdProduct($_SESSION['idProduct']);

if(isset($_POST['submitRecension'])) {
    $user = $db->getUserById($_SESSION['email']);
    $date = date("Y-m-d");
    $db->setRecension($product->getId(),$user->getIdUser(),$_POST['text-recension'],$date);
    echo '<script type="text/javascript"> openRecenziePost()</script>';
}

if(isset($_POST['addToCart']))
{
    $amount = $_POST['amountValue'];
    $shoppingCart->addToShoppingCart($product,$amount);
    echo '<script type="text/javascript"> window.location = "Detail.php"</script>';
}

if(isset($_POST['del']))
{
    $id = $_POST['deleted'];
    if($db->deleteRecension($id))
    {
        echo "Vymazane";
    }
}

?>
    <script>
        function openDetailPost(){
            document.getElementById("recenzie-post").style.display = "none";
            document.getElementById("detail-post").style.display = "block";

        }
        function openRecenziePost(){
            document.getElementById("detail-post").style.display = "none";
            document.getElementById("recenzie-post").style.display = "block";
        }


        function compute()
        {
            let x = $('#amount').val();
            let y = $('#price').text();
            $('#together').html(x*y);
        }

        $(document).ready(function () {
            $(".removeRecension").on('click',function (){
                var id = $(this).attr('id');
                console.log(id);

                $.ajax({
                    type:'POST',
                    url:'detailZalozny.php',
                    data:{
                        del:1,
                        deleted:id
                    },
                    success: function(){
                        $("#recens"+id).remove();
                    }
                })
            })

        })

    </script>
    <main>
        <div class="container detail-main">
            <div  class="row">
                <div id="detail-main-panel" class="col-12">
                    <div>
                        <div class="detail-intro">
                            <hr>
                            <div style="padding-top: 20px">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">

                                            <div class="carousel-inner" role="listbox">
                                                <?php
                                                $i = 0;
                                                foreach ($photos as $photo) {
                                                    if($i == 0) {
                                                        ?>
                                                        <div class="carousel-item active">
                                                            <img class="d-block w-100 h-100"
                                                                 src="img/<?php echo $product->getIdCategory() ?>/<?php echo $product->getId() ?>/<?php echo $photo->getFile() ?>"
                                                                 alt="">
                                                        </div>

                                                <?php
                                                    }else {
                                                        ?>
                                                        <div class="carousel-item ">
                                                            <img class="d-block w-100 h-100"
                                                                 src="img/<?php echo $product->getIdCategory() ?>/<?php echo $product->getId() ?>/<?php echo $photo->getFile() ?>"
                                                                 alt="">
                                                        </div>
                                                <?php } $i++;
                                                } ?>
                                            </div>
                                            <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <h2><strong><?php echo $product->getName(); ?></strong> </h2>
                                        <div class="stars">
                                            <i class="fa fa-star star-active"></i>
                                            <i class="fa fa-star star-active"></i>
                                            <i class="fa fa-star star-active"></i>
                                            <i class="fa fa-star star-active"></i>
                                            <i class="fa fa-star star-active"></i>
                                        </div>
                                        <p><strong>Cena:  <span id="price"><?php echo $product->getPrice() ?></span> €</strong> </p>
                                        <p class="description"><?php echo $product->getDescription(); ?>
                                        </p>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="buy-div">
                                                    <div class=" row">
                                                        <label class="col-form-label col-sm-3 col-lg-3">MNOZSTVO:</label>
                                                        <div class="col-sm-9">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <form method="post" >
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <input type="number" value="1" min="1" class="form-control inp" id="amount" name="amountValue" onchange="compute()">
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <input  class="btn cart px-auto abc" type="submit" name="addToCart" value="Pridať do košíka">
                                                                            </div>
                                                                        </div>

                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row det-d">
                                            <div class="col">
                                                <div class="total-price">
                                                    Spolu:
                                                    <span id="together"><?php echo $product->getPrice() ?></span>
                                                    €
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12" id="productDetail">
                            <div>
                                <nav>
<!--                                    //role="tab"   data-toggle="tab"  aria-controls="prodDescription" aria-selected="true"  -->
                                    <div class="nav nav-tabs  detail-head">
                                        <a href="#" class="nav-item nav-link active show"  onclick="openDetailPost()">
                                            <span>Detail o produkte</span>
                                        </a>
                                        <a href="#" class="nav-item nav-link"  onclick="openRecenziePost()">
                                            <span>Recenzie</span>
                                        </a>
                                    </div>
                                </nav>

                                <div class="detail-post" id="detail-post">
                                    <div class="row ">
                                        <div class="col-12 ">
                                            <h3><strong>Zloženie</strong></h3>
                                            <p class="description des-p" >
                                                <?php echo $product->getComposition()?>
                                            </p >
                                            <h3>Chuť kávy</h3>
                                            <p class="description des-p">
                                                <?php echo $product->getTaste()?>
                                            </p>

                                            <h3>Pôvod kávy</h3>
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <p class="description des-p">
                                                        <?php echo $product->getOrigin()?>
                                                    </p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <img style="padding-bottom: 10px"  class="des-p" src="img/Origin/<?php echo $product->getId()?>.png" alt="">
                                                </div>
                                            </div>

                                            <h3>Návod na prípravu</h3>
                                            <p class="description des-p">
                                                <?php echo $product->getInstruction()?>
                                            </p>

                                        </div>
                                    </div>
                                </div>

                                <div class="detail-post" id="recenzie-post">
                                    <div class="row ">
                                        <div class="col-12">
                                            <div class="row ">
                                                <?php
                                                    if(isset($_SESSION['alreadyLogged']))
                                                    { ?>
                                                        <div class="col-sm-12 text-center">
                                                            <div>
                                                                <h4>Pomožte ostatným pri výbere</h4>
                                                                <form  class="settings-container" method="post" id="">
                                                                    <input type="text" class="form-control" name="text-recension" id="text-recension" value="">
                                                                    <input  class="btn cart px-auto" type="submit" name="submitRecension" value="NAPISAT RECENZIU">
                                                                </form>
                                                            </div>
                                                        </div>
                                                 <?php } else
                                                     { ?>
                                                        <div class="col-sm-12">
                                                            Ak chcete uverejniť recenziu, prosím <a href="login.php"> prihláste sa</a>
                                                        </div>


                                                <?php
                                                }
                                                if(count($recensions) >0) {
                                                foreach ($recensions as $recension) {
                                                    $user = $db->getUserById2($recension->getIdUser())?>
                                                <div class="col-sm-12">
                                                    <div class="recens" id="recens<?php echo $recension->getIdRecenzie()?>">
                                                        <blockquote>
                                                            <span class="recens-text"><?php echo $recension->getText()?></span>
                                                            <span class="recens-name"><?php echo $user->getName()?> <?php echo $user->getSurname()?></span>
                                                            <span class="recens-date"><?php echo $recension->getDate()?></span>
                                                            <?php
                                                            if (isset($_SESSION['email']))
                                                            {
                                                                if($db->getIdUser($_SESSION['email']) === $recension->getIdUser()) {?>
                                                                    <p>(Vaša recenzia)</p>
                                                                    <i class="fa fa-trash-o site-icon removeRecension" id="<?php echo $recension->getIdRecenzie()?>"></i>
                                                                <?php }} ?>
                                                        </blockquote>
                                                    </div>
                                                </div>
                                                <?php }
                                                }else { ?>
                                                    <div class="col-sm-12 text-center">
                                                        <p class="no-recens">
                                                            K tomuto produktu zatial niesú žiadne recenzie.
                                                        </p>
                                                    </div>
                                                <?php }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>>
    </main>

<?php
include "footer.php";
?>