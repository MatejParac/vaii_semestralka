<?php
include "header.php";
?>

<header class="masthead text-center">
    <div class="masthead-content" style="margin-top: 50px;">
        <div class="container">
            <h1 class="masthead-heading mb-0 ">O nás</h1>
            <h2 class="masthead-subheading mb-0"></h2>
<!--            <a href="#" class="btn btn-primary btn-xl rounded-pill mt-5">Learn More</a>-->
        </div>
    </div>
    <div class="bg-circle-1 bg-circle"></div>
    <div class="bg-circle-2 bg-circle"></div>
    <div class="bg-circle-3 bg-circle"></div>
    <div class="bg-circle-4 bg-circle"></div>
</header>

<section>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
                <div class="p-5">
                    <img class="img-fluid rounded-circle" src="img/aboutus1.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6 order-lg-1">
                <div class="p-5">
                    <h2 class="display-4">Pre tých, ktorí milujú kávu</h2>
                    <p>Milujete kávu intenzívnu, okrúhlu, celistvú, provokatívnu či elegantnú? Máme! Prinášame vám presne to, čo žiadajú vaše kultivované chuťové bunky.

                        Už nikdy sa vám voňavá káva nevysype do šuplíka alebo snáď kabelky. Znovu uzatvárateľné balenie je praktické a bezpečné.

                        Naše etikety sú krásne. Budete si nimi chcieť polepiť pol kuchyne. No, to nie, ale na plechovku s kávou sa hodia. Pošleme za symbolický poplatok.

                        Ak máte dobré srdce, že sa podelíte o posledné kávové zrnko, pribalíme vám sáčok navyše. Za výrobnú cenu.

                        Každý miluje darčeky. Dajte nám adresu a my za vás kávu doručíme. Fakturácia samozrejme zostane len medzi nami.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="p-5">
                    <img class="img-fluid rounded-circle" src="img/aboutus2.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="p-5">
                    <h2 class="display-4">Ďakujeme Vám!</h2>
                    <p>Svieži, kreatívni, zodpovední. Takí sú naši zákazníci a takí sme aj my. Hodnoty tohto konceptu budujeme na najvyššej kvalite, spoľahlivom a zdvorilom partnerstve s farmármi, ktorí nás zásobujú výberovou kávou.</p>
                </div>
            </div>
        </div>
    </div>
</section>

