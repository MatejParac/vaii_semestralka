<?php
include "header.php";
?>

<section class="page-section about-heading">
    <div class="container">
        <img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="img/kolumbia.jpg" alt="">
        <div class="about-heading-content">
            <div class="row">
                <div class="col-xl-9 col-lg-10 mx-auto">
                    <div class="bg-faded rounded p-5">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Strong Coffee, Strong Roots</span>
                            <span class="section-heading-lower">O našej káve</span>
                        </h2>
                        <p>Naša káva má svoj príbeh. Chceme, aby ste vo svojej šálke dostali 100 % kvalitu zŕn. Preto dozeráme na každý jeden krok, od pestovania až po finálnu prípravu.</p>
                        <p class="mb-0"> A práve toto je to miesto, kde sa o celom procese dozviete viac.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="page-section" id="about">
    <div class="container">
<!--        <div class="text-center">-->
<!--            <h2 class="section-heading text-uppercase">About</h2>-->
<!--            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>-->
<!--        </div>-->
        <ul class="timeline">
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://becafe.sk/wp-content/uploads/2019/01/AdobeStock_74955106.jpeg" alt="" /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">

                        <h2 class="subheading">Pestovanie a zber</h2>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Začiatky sú dôležité</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://www.univerzitakavy.sk/wp-content/uploads/2018/04/15964884_xxl-1.jpg" alt="" /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h2 class="subheading">Spracovanie</h2>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Prispôsobené vašej chuti</p></div>
                </div>
            </li>
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://www.coffeein.sk/images/article/pestovatelske_krajiny/salvador/salvador-kava-1.jpg" alt="" /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h2 class="subheading">Sušenie</h2>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Starostlivosťou ku kvalite</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://i2.wp.com/www.sscoffee.sk/wp-content/uploads/2015/06/11097_431578973656482_6476308491998410685_n.jpg?fit=800%2C533&ssl=1" alt="" /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="subheading">Uskladnenie</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Na ružiach ustlané</p></div>
                </div>
            </li>
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLw3Qgaa_ymWGQZ7W-7iedxDErlJnpDC1t9A&usqp=CAU" alt="" /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h2 class="subheading">TRANSPORT</h2>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Za vami aj na kraj sveta</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://www.okave.sk/wp-content/uploads/2018/11/lsdhjlasdlasdjlhasd-640x336.jpg" alt="" /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="subheading">PRAŽENIE</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Odborne a s citom</p></div>
                </div>
            </li>
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://img.klocher.sk/wp-content/uploads/2018/07/alternat%C3%ADvne-sp%C3%B4soby-pr%C3%ADpravy-k%C3%A1vy-e1532077163900.jpg" alt="" /></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h2 class="subheading">PRÍPRAVA</h2>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Pre vaše krásne dni</p></div>
                </div>
            </li>
<!---->
<!---->
<!--            <li class="timeline-inverted">-->
<!--                <div class="timeline-image">-->
<!--                    <h4>-->
<!--                        Be Part-->
<!--                        <br />-->
<!--                        Of Our-->
<!--                        <br />-->
<!--                        Story!-->
<!--                    </h4>-->
<!--                </div>-->
<!--            </li>-->
        </ul>
    </div>
</section>