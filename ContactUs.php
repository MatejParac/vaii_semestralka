<?php
include "header.php";

if(isset($_POST['sendM']))
{
    $emailTo = "paracmatej@gmail.com";
    $header = "Správa od: " . $_POST['name_'] . ", Email: " . $_POST['email_'] . " ";
    if(mail($emailTo,$_POST['subject_'],$_POST['message_'],$header))
    {
        echo " Správa úspešne odoslaná" ;
    }else
        echo " Správa nebola úspešne odoslaná" ;

//        echo "Ahoj";
//        exit();
}


?>

<script>
    $(document).ready(function () {
        $("#sendMessageButton").on('click',function(e){
            e.preventDefault();
            var name = $("#name").val();
            var email = $("#email").val();
            var subject = $("#subject").val();
            var message = $("#message").val();

            if(name=="" || email==""|| subject=="" || message=="")
            {
                $("#response").html("Prosím vyplň všetky polia");
            }else
            {
                $.ajax({
                    url:'ContactUs.php',
                    type:'POST',
                    data:{
                        sendM:1,
                        name_:name,
                        email_:email,
                        subject_:subject,
                        message_:message
                    },

                    success:function (response) {
                        $("#response").html(response);
                    },
                    dataType:'text'

                })
            }

        })

    })
</script>



<section class="page-section cntctUs" id="contact">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Kontakt</h2>
            <h3 class="section-subheading text-muted">Ak sa vyskytol nejaký problém, prosím kontaktujte nás</h3>
        </div>
        <form id="contactForm" name="sentMessage"  method="post" >
            <div class="row align-items-stretch mb-5">
                <div class="col-md-6">
                    <div class="form-group">
                        <input class="form-control" id="name" type="text" placeholder="Meno *" required>

                    </div>
                    <div class="form-group">
                        <input class="form-control" id="email" type="email" placeholder="Email *" required >
                    </div>
                    <div class="form-group mb-md-0">
                        <input class="form-control" id="subject" type="text" placeholder="Predmet *" required >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-textarea mb-md-0">
                        <textarea class="form-control" id="message" placeholder="Vaša správa *" required></textarea>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <span class="group-btn">
                    <input  class="btn cart px-auto" type="submit" name="sendMessageB" value="Odoslať správu" id="sendMessageButton">
                </span>
            </div>
        </form>
        <div><p id="response"></p></div>
    </div>
</section>







