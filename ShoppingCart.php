<?php
include "header.php";

$_SESSION['shipping']= 0;
$_SESSION['totalpriceWithShipping']= 0;

if(isset($_GET['code']))
{
    $shoppingCart->removeProductFromCart($_GET['code']);
    echo '<script type="text/javascript"> window.location = "ShoppingCart.php"</script>';
}

if(isset($_POST['removeCart']))
{
    $shoppingCart->removeCart();
    echo '<script type="text/javascript"> window.location = "ShoppingCart.php"</script>';
}

?>

<main>
    <?php
    if($shoppingCart->getAllProductInCart() == 0)
    {
        echo " 
                <div class=\"container\">
                    <div class=\"row justify-content-center\">
                        <div class=\"col-sm-8\">
            
                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <p>Tvoj nákupný košík je prázdy</p>
                                </div>
                            </div>
            
                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <ul>
                                        <li>Doprava zadarmo nad 50 €</li>
                                        <li>Dobierka</li>
                                        <li>Rýchle doručenie</li>
                                    </ul>
                                </div>
                            </div>
            
                            <div class=\"row justify-content-center\">
                                <div class=\"col-sm-5\">
                                    <a href=\"index.php#Offers\"><input  class=\"btn cart px-auto\" type=\"submit\" name=\"continue\" value=\"Pokračovať v nákupe\"></a>
                                </div>
                                <div class=\"col-sm-5\">
                                    <div class=\" emptyCart\"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                ";
    }else{
        echo "
                 <div class=\"container\">
                    <div class=\"row justify-content-center\">
                        <div class=\"col-md-10\">
                            <div class=\"row\">                                                     
                                <div class=\"col-md-7\">
                                    <div class='row'>
                                        <div class='col-sm-6 '>
                                            <p class='shopping-cart-title'>Nákupný košík (". $shoppingCart->getCountOfProduct() .")</p>                                      
                                        </div>
                                        <div class='col-sm-6 d-flex justify-content-end align-items-center'> 
                                        <form method='post'>
                                            <button name='removeCart' class='remove-all-cart'><i class=\"fa fa-times remove-item\" > Odstrániť celý košík</i></button>    
                                        </form>                                                                
                                        </div>
                                    </div>
                                    
                                    <ul class=\"shoping-cart-ul\"> ";
        foreach ($shoppingCart->getAllProductInCart() as $id=>$item) {
            $photo = $db->getPhotosByIdProduct($shoppingCart->getAllProductInCart()[$id]['id']);
            echo
                "                                  
                                            <li class=\"shoping-cart-item\">
                                                <div class=\"shoping-cart-order\">
                                                    <div class=\"shoping-cart-order-details\">
                                                        <div class=\"shoping-cart-image\">
                                                            <img src=\"img/".$shoppingCart->getAllProductInCart()[$id]['id_category']."/".$shoppingCart->getAllProductInCart()[$id]['id']."/".$photo[0]->getFile()."\" alt=\"\">
                                                        </div>
                
                                                        <div class=\"shopping-cart-o-d-n\">
                                                            <div>
                                                                <div class='shoping-cart-product-name'> <strong>" . $shoppingCart->getAllProductInCart()[$id]['name'] . "</strong> </div>
                                                                <div class='shoping-cart-product-name'> " . $db->getNameOfCategory($shoppingCart->getAllProductInCart()[$id]['id_category'])   . "</div>
                                                            </div>
                                                            <div class='sc-remove'>
                                                                <a href=\"ShoppingCart.php?code=" . $shoppingCart->getAllProductInCart()[$id]['id']."\" >                                                                     
                                                                     <i class=\"fa fa-times remove-item\" > Odstrániť</i>
                                                                </a>                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=\"shopping-cart-price\">          
                                                            <div class=\"s-c-count\">
                                                                " . $shoppingCart->getAllProductInCart()[$id]['amount'] . " 
                                                            </div>
                                                            <div class=\"s-c-price\">
                                                                " . $shoppingCart->getAllProductInCart()[$id]['price']*$shoppingCart->getAllProductInCart()[$id]['amount']  . " €
                                                            </div>
                                                    </div>
                                                </div>
                                            </li>     
                                        ";
        }
        if($_SESSION['totalprice'] < 50)
        {
            $_SESSION['shipping'] = 4;
        }else
        {
            $_SESSION['shipping'] = 0;
        }
        $_SESSION['totalpriceWithShipping'] = $_SESSION['totalprice']+ $_SESSION['shipping'];
        echo "                                       
                                    </ul>
                                </div>  
                                                                           
                                 <div class=\"col-md-5\">
                                    <div class=\"shoping-cart-overall\">
                                        <div> <h3> <strong>Suma</strong> </h3></div>
                                        <table class='tab'>
                                            <tr class=\"sumary\">
                                                <td class=\"sumary-a\">Suma za produkty</td>
                                                <td class=\"sumary-b\">". $_SESSION['totalprice'] ." €</td>
                                            </tr>
                                            <tr class=\"sumary bcf\">
                                                <td>Doprava</td>
                                                <td class=\"sumary-b\"> ". $_SESSION['shipping'] ."  €</td>
                                            </tr>       
                                            <tr class=\"gh\">
                                                <td class=\"sumary-a \"><strong>Celková suma</strong> </td>
                                                <td class=\"sumary-b\"><strong>". $_SESSION['totalpriceWithShipping'] ." €</strong></td>
                                            </tr>
                                        </table>
                                        <div>
                                            <a href='checkout.php'>
                                                <input  class=\"btn cart px-auto\" type=\"submit\" name=\"registerButton\" value=\"Dokončiť objednávku\">
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            ";
    }
    ?>

</main>
