<?php
session_start();
unset($_SESSION['alreadyLogged']);
unset($_SESSION['email']);
unset($_SESSION['id_logged_user']) ;
session_destroy();
header('Location:index.php');
exit();
?>