<?php
session_start();
include("app/classes/DbStorage.php");

$db = new DbStorage();
$msg="";

if(isset($_POST["do_login"]))
{
    $email = $_POST['username'];
    $password = $_POST['password'];

    $msg = $db->loginUser($email,$password);
    if($msg=="Uspesne prihlaseny")
    {
        echo "Ok";
    }elseif ($msg =="Zadali ste zle heslo") {
        echo "Zadali ste zle heslo";
    }else{
        echo "Taky uzivatel neexistuje";
    }
    exit();
}

?>


<!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
<!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>-->
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords"
          content="unique login form,leamug login form,boostrap login form,responsive login form,free css html login form,download login form">
    <meta name="author" content="leamug">
    <title>E-shop-Prihlásenie</title>
    <link href="css/style.css" rel="stylesheet" id="style">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="https://cpwebassets.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico">

</head>
<body class="login_body">

<script>
    $(document).ready(function () {
        $("#loginButt").on('click',function (e){
            e.preventDefault();
            var username = $("#email").val();
            var password = $("#password").val();

            if(username=="" || password=="")
            {
                $("#response").html("Prosím vyplň všetky polia");
            }else
            {
                $.ajax(
                    {
                        url: 'login.php',
                        type: 'POST',
                        data: {
                            do_login:"do_login",
                            username: username,
                            password: password
                        },
                        success: function (response) {

                            if(response === "Ok"){
                                window.location.href = "index.php";
                            }else if (response==="Zadali ste zle heslo") {
                                //alert("Zadali ste zle heslo");
                                $("#response").html(response);
                            }else{
                                //alert("Taky pouzivatel neexistuje");
                                $("#response").html(response);
                            }
                        },
                        dataType:'text'
                    });
            }
        })
    });

</script>

<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-4 text-center lgn">
            <div class="form-login">
                <div class="lr-list">
                    <a href="" class="btn  btn-lg active" role="button" aria-pressed="true"  id="loginButton">Prihlásenie</a>
                    <a href="register.php" class="btn  btn-lg active" role="button" aria-pressed="true"  id="registerButton">Registrácia</a>
                </div>

                <form  method="post">
                    <input type="email" class="form-control" name="email" id="email"  placeholder="Email" required>

                    <input type="password" class="form-control" name="password" id="password" placeholder="Heslo" required>

                    <div class="wrapper">
                        <span class="group-btn">
                            <input  class="btn cart px-auto" type="submit" name="loginButton" value="Prihlásiť" id="loginButt">
                        </span>
                    </div>
                </form>
                <p id="response" class="response-logreg"></p>
                <a href="index.php" >
                    <span class="fa fa-chevron-left">Pokračovať bez prihlásenia</span>
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
